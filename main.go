package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// Location struct to map the entire response
type Location struct {
	Status string `json:"status"`
	Data   Data   `json:"data"`
}

// Data object to hold the response data
type Data struct {
	Ipv4             string `json:"ipv4"`
	ContinentName    string `json:"continent_name"`
	CountryName      string `json:"country_name"`
	Subdivision1Name string `json:"subdivision_1_name"`
	Subdivision2Name string `json:"subdivision_2_name"`
	CityName         string `json:"city_name"`
	Latitude         string `json:"latitude"`
	Longitude        string `json:"longitude"`
}

// WeatherData main holder
type WeatherData struct {
	Coord      Coord     `json:"coord"`
	Weather    []Weather `json:"weather"`
	Base       string    `json:"base"`
	Main       Main      `json:"main"`
	Visibility int       `json:"visibility"`
	Wind       Wind      `json:"wind"`
	Clouds     Clouds    `json:"clouds"`
	Dt         int       `json:"dt"`
	Sys        Sys       `json:"sys"`
	Timezone   int       `json:"timezone"`
	ID         int       `json:"id"`
	Name       string    `json:"name"`
	Cod        int       `json:"cod"`
}

// Coord the lat and long
type Coord struct {
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}

// Weather info
type Weather struct {
	ID          int    `json:"id"`
	Main        string `json:"main"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
}

// Main weather information
type Main struct {
	Temp     float64 `json:"temp"`
	Pressure int     `json:"pressure"`
	Humidity int     `json:"humidity"`
	TempMin  float64 `json:"temp_min"`
	TempMax  float64 `json:"temp_max"`
}

// Wind information
type Wind struct {
	Speed float64 `json:"speed"`
	Deg   int     `json:"deg"`
}

// Clouds information
type Clouds struct {
	All int `json:"all"`
}

// Sys information about the system
type Sys struct {
	Type    int     `json:"type"`
	ID      int     `json:"id"`
	Message float64 `json:"message"`
	Country string  `json:"country"`
	Sunrise int     `json:"sunrise"`
	Sunset  int     `json:"sunset"`
}

var (
	apiKey = "75efd991eb399fc3144776aab2a7aa4e"
)

func main() {
	response, err := http.Get("https://ipvigilante.com/")

	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var locationObject Location
	json.Unmarshal(responseData, &locationObject)

	var url = fmt.Sprintf("http://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&APPID=%s&units=imperial", locationObject.Data.Latitude, locationObject.Data.Longitude, apiKey)

	response, err = http.Get(url)

	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	responseData, err = ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var weatherObject WeatherData
	json.Unmarshal(responseData, &weatherObject)

	var output = fmt.Sprintf("It is currently %v with a high of %v and a low of %v", weatherObject.Main.Temp, weatherObject.Main.TempMax, weatherObject.Main.TempMin)

	fmt.Println(output)

}
